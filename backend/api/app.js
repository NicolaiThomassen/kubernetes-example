const express = require('express');
const app = express();
const port = 3000;

const randomNumber = Math.ceil(Math.random() * 100);

app.get('/', (req, res) => {
  res.json({
    number: randomNumber
  });
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
})